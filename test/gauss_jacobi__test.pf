!> summary:  Unit test for Gauss/Lobatto/Radau-Routines from the Gauss_Jacobi
!> author:   Susanne Stimpert, Joerg Stiller
!> date:     2015/01/23
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!>
!> Test of the functions {Gauss|Lobatto}{Points|Weights|DiffMatrix|Polynomial}
!> from the Gauss_Jacobi module in [-1, 1] with the polynomial degree p ranging
!> from 1 to 15. TestFunc is a non-symmmetric polynomial test function of degree
!> p which satisfies
!>
!>   *  \( TestFunc(-1, p) = TestFunc(1, p) = 0 \)  and
!>
!>   *  \( \int_{-1}^1 TestFunc(x, p) dx = 1 \).
!>
!===============================================================================

module Gauss_Jacobi__Test

  use Kind_Parameters, only: RNP
  use Constants,       only: ZERO, ONE, TWO, PI, THIRD
  use Gauss_Jacobi
  use Pfunit_Mod

  implicit none

  real(RNP), parameter :: TOL = 1e-11_RNP
  integer,   parameter :: P_MAX = 15
  real(RNP)            :: deviation

contains

@test

!-------------------------------------------------------------------------------
!> Test of the functions GaussPoints and GaussDiffMatrix

subroutine GaussDifferentiation()

  integer :: p, i
  real(RNP), allocatable :: x(:), f(:), df(:), df_exact(:), d(:,:)

  write(*,'(A)') 'Testing Gauss differentiation'

  do p = 1, P_MAX
     allocate(x(0:p), f(0:p), df(0:p), df_exact(0:p), d(0:p, 0:p))
     x = GaussPoints(p)
     d = GaussDiffMatrix(x)

     do i = 0, p
        f(i) = TestFunc(x(i), p)
     end do

     do i = 0, p
        df(i) = sum(f * d(i,:))
     end do

     df_exact  = [(DTestFunc(x(i), p), i = 0, p)]
     deviation = maxval(abs(df_exact -df))
     @assertEqual(deviation, ZERO, TOL)

     deallocate(x, f, df, df_exact, d)
  end do

end subroutine GaussDifferentiation

@test

!-------------------------------------------------------------------------------
!> Test of the functions LobattoPoints and LobattoDiffMatrix

subroutine LobattoDifferentiation()

  integer :: p, i
  real(RNP), allocatable :: x(:), f(:), df(:), df_exact(:), d(:,:)

  write(*,'(A)') 'Testing Lobatto differentiation'

  do p = 1, P_MAX
     allocate(x(0:p), f(0:p), df(0:p), df_exact(0:p), d(0:p, 0:p))
     x = LobattoPoints(p)
     d = LobattoDiffMatrix(x)

     do i = 0, p
        f(i) = TestFunc(x(i), p)
     end do

     do i = 0, p
        df(i) = sum(f * d(i,:))
     end do

     df_exact = ([(DTestFunc(x(i), p), i = 0, p)])
     deviation = maxval(abs(df_exact -df))
     @assertEqual(deviation, ZERO, TOL)

     deallocate(x, f, df, df_exact, d)
  end do

end subroutine LobattoDifferentiation

@test

!-------------------------------------------------------------------------------
!> Test of the functions RadauPoints and RadauDiffMatrix

subroutine RadauDifferentiation()

  integer :: p, i
  real(RNP), allocatable :: x(:), f(:), df(:), df_exact(:), d(:,:)

  write(*,'(A)') 'Testing Radau differentiation'

  do p = 1, P_MAX
     allocate(x(0:p), f(0:p), df(0:p), df_exact(0:p), d(0:p, 0:p))
     x = RadauPoints(p)
     d = RadauDiffMatrix(x)

     do i = 0, p
        f(i) = TestFunc(x(i), p)
     end do

     do i = 0, p
        df(i) = sum(f * d(i,:))
     end do

     df_exact = ([(DTestFunc(x(i), p), i = 0, p)])
     deviation = maxval(abs(df_exact -df))
     @assertEqual(deviation, ZERO, TOL)

     deallocate(x, f, df, df_exact, d)
  end do

end subroutine RadauDifferentiation

@test

!-------------------------------------------------------------------------------
!> Test of the functions GaussPoints and GaussWeights

subroutine GaussIntegration()

  integer :: p, i
  real(RNP), allocatable :: x(:), w(:)
  real(RNP) :: Inum

  write(*,'(A)') 'Testing Gauss quadrature'

  do p = 1, P_MAX
     allocate(x(0:p), w(0:p))
     x = GaussPoints(p)
     w = GaussWeights(x)

     Inum = sum([ (TestFunc(x(i), p) * w(i), i = 0, p) ])
     deviation = abs(1 - Inum)
     @assertEqual(deviation, ZERO, TOL)

     deallocate(x, w)
  end do

end subroutine GaussIntegration

@test

!-------------------------------------------------------------------------------
!> Test of the functions LobattoPoints and LobattoWeights

subroutine LobattoIntegration()
  integer :: p, i
  real(RNP), allocatable :: x(:), w(:)
  real(RNP) :: Inum

  write(*,'(A)') 'Testing Lobatto quadrature'

  do p = 1, P_MAX
     allocate(x(0:p), w(0:p))
     x = LobattoPoints(p)
     w = Lobattoweights(x)

     Inum = sum([ (TestFunc(x(i), p) * w(i), i = 0, p) ])
     deviation = abs(1 - Inum)
     @assertEqual(deviation, ZERO, TOL)

     deallocate(x, w)
  end do

end subroutine LobattoIntegration

@test

!-------------------------------------------------------------------------------
!> Test of the functions RadauPoints and RadauWeights

subroutine RadauIntegration()

  integer :: p, i
  real(RNP), allocatable :: x(:), w(:)
  real(RNP) :: Inum

  write(*,'(A)') 'Testing Radau quadrature'

  do p = 1, P_MAX
     allocate(x(0:p), w(0:p))
     x = RadauPoints(p)
     w = Radauweights(x)

     Inum = sum([ (TestFunc(x(i), p) * w(i), i = 0, p) ])
     deviation = abs(1 - Inum)
     @assertEqual(deviation, ZERO, TOL)

     deallocate(x, w)
  end do

end subroutine RadauIntegration

@test

!-------------------------------------------------------------------------------
!> Test of the functions GaussPoints and GaussPolynomial

subroutine GaussInterpolation()
  integer :: p, i
  real(RNP), allocatable :: x(:)
  real(RNP) :: f_int, x_int

  write(*,'(A)') 'Testing Gauss interpolation'

  do p = 1, P_MAX
     allocate(x(0:p))
     x = GaussPoints(p)
     x_int = THIRD * (TWO * x(p/2) + x(p/2 + 1))

     f_int = 0
     do i = 0, p
        f_int = f_int + TestFunc(x(i), p) * GaussPolynomial(i, x, x_int)
     end do

     deviation = abs(TestFunc(x_int, p) - f_int )
     @assertEqual(deviation, ZERO, TOL)

     deallocate(x)
  end do

end subroutine GaussInterpolation

@test

!-------------------------------------------------------------------------------
!> Test of the functions LobattoPoints and LobattoPolynomial

subroutine LobattoInterpolation()
  integer :: p, i
  real(RNP), allocatable :: x(:)
  real(RNP) :: f_int, x_int

  write(*,'(A)') 'Testing Lobatto interpolation'

  do p = 1, P_MAX
     allocate(x(0:p))
     x = LobattoPoints(p)
     x_int = THIRD * (TWO * x(p/2) + x(p/2 + 1))

     f_int = 0
     do i = 0, p
        f_int = f_int + TestFunc(x(i), p) * LobattoPolynomial(i, x, x_int)
     end do

     deviation = abs(TestFunc(x_int, p) - f_int)
     @assertEqual(deviation, ZERO, TOL)

     deallocate(x)
  end do

end subroutine LobattoInterpolation

@test

!-------------------------------------------------------------------------------
!> Test of the functions RadauPoints and RadauPolynomial

subroutine RadauInterpolation()
  integer :: p, i
  real(RNP), allocatable :: x(:)
  real(RNP) :: f_int, x_int

  write(*,'(A)') 'Testing Radau interpolation'

  do p = 1, P_MAX
     allocate(x(0:p))
     x = RadauPoints(p)
     x_int = THIRD * (TWO * x(p/2) + x(p/2 + 1))

     f_int = 0
     do i = 0, p
        f_int = f_int + TestFunc(x(i), p) * RadauPolynomial(i, x, x_int)
     end do

     deviation = abs(TestFunc(x_int, p) - f_int)
     @assertEqual(deviation, ZERO, TOL)

     deallocate(x)
  end do

end subroutine RadauInterpolation

!===============================================================================
! test function

!-------------------------------------------------------------------------------
!> Test function
!>
!> Polynomial test function of degree p in [-1,1] satisfying
!>
!>   * TestFunc(-1, p) = TestFunc(1, p) = 0,
!>
!>   * \int_{-1}^1 TestFunc(x, p) dx = 1

elemental function TestFunc(x, p) result(y)
  real(RNP), intent(in) :: x  !< position in [-1,1]
  integer  , intent(in) :: p  !< polynomial degree
  real(RNP) :: y

  real(RNP) :: a, b

  a = ONE/((-2)**(p + 1)*(1 - ONE/(p + 1)))
  b = - a*(-2)**p

  y = a*(x-1)**p + b

end function TestFunc

!-------------------------------------------------------------------------------
!> Derivative of the test function

elemental function DTestFunc(x, p) result(y)
  real(RNP), intent(in) :: x  !< position in [-1,1]
  integer, intent(in)   :: p  !< polynomial degree
  real(RNP) :: y

  real(RNP) :: a

  a = ONE/((-2)**(p + 1) * (1 - ONE/(p + 1)))
  y = a * p * (x - 1)**(p - 1)

end function DTestFunc

!===============================================================================

end module  Gauss_Jacobi__Test
