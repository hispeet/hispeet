!> summary:  3D rotation element operator using LIBXSSM (R)
!> author:   Erik Pfister
!> date:     2019/11/06
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Rot__3D_R__XSMM
  use TPO__Rot__3D_R__XSMM_RDP
end module TPO__Rot__3D_R__XSMM
