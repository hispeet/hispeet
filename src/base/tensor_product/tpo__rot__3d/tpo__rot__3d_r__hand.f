!> summary:   Rotation element operator based on handcrafted kernels (R)
!> author:    Joerg Stiller
!> date:      2021/06/09
!> license:   Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Rot__3D_R__Hand
  use TPO__Rot__3D_R__Hand_RDP
end module TPO__Rot__3D_R__Hand
