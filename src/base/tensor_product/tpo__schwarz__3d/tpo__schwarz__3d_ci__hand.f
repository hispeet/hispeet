!> summary:   3d isotropic Schwarz operator based on handcrafted kernels (CI)
!> author:    Joerg Stiller, Erik Pfister
!> date:      2020/06/02
!> license:   Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Schwarz__3D_CI__Hand
  use TPO__Schwarz__3D_CI__Hand_RSP
  use TPO__Schwarz__3D_CI__Hand_RDP
end module TPO__Schwarz__3D_CI__Hand
