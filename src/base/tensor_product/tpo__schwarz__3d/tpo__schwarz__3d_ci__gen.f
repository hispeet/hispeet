!> summary:  3d generic isotropic Schwarz operator
!> author:   Joerg Stiller
!> date:     2020/06/02
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Schwarz__3D_CI__Gen
  use TPO__Schwarz__3D_CI__Gen_RSP
  use TPO__Schwarz__3D_CI__Gen_RDP
end module TPO__Schwarz__3D_CI__Gen
