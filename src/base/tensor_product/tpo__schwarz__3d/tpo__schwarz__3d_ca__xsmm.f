!> summary:  3d generic anisotropic Schwarz operator using LIBXSMM
!> author:   Joerg Stiller, Erik Pfister
!> date:     2020/06/02
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Schwarz__3D_CA__XSMM
  use TPO__Schwarz__3D_CA__XSMM_RSP
  use TPO__Schwarz__3D_CA__XSMM_RDP
end module TPO__Schwarz__3D_CA__XSMM
