!> summary:   element diffusion operator based on handcrafted kernels (RLCI)
!> author:    Joerg Stiller
!> date:      2021/06/09
!> license:   Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Diffusion__3D_RLCI__Hand
  use TPO__Diffusion__3D_RLCI__Hand_RDP
end module TPO__Diffusion__3D_RLCI__Hand
