!> summary:  3d generic element diffusion operator (RLVI)
!> author:   Karl Schoppmann, Joerg Stiller
!> date:     2018/11/20
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Diffusion__3D_RLVI__Gen
  use TPO__Diffusion__3D_RLVI__Gen_RDP
end module TPO__Diffusion__3D_RLVI__Gen
