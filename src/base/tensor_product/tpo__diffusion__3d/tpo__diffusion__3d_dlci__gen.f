!> summary:  3d curvilinear generic element diffusion operator (DLCI)
!> author:   Jerome Michel, Jörg Stiller
!> date:     2021/07/2021
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Diffusion__3D_DLCI__Gen
  use TPO__Diffusion__3D_DLCI__Gen_RDP
end module TPO__Diffusion__3D_DLCI__Gen
