!> summary:  3D element diffusion operators
!> author:   Joerg Stiller
!> date:     2020/05/29
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Diffusion__3D
  use TPO__Diffusion__3D_RLCI
  use TPO__Diffusion__3D_RLVI
  use TPO__Diffusion__3D_DLCI
end module TPO__Diffusion__3D
