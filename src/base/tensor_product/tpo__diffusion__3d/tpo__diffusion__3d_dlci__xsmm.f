!> summary:  3D element diffusion operator using LIBXSMM (DLCI)
!> author:   Jerome Michel, Joerg Stiller
!> date:     2021/08/19
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Diffusion__3D_DLCI__XSMM
  use TPO__Diffusion__3D_DLCI__XSMM_RDP
end module TPO__Diffusion__3D_DLCI__XSMM
