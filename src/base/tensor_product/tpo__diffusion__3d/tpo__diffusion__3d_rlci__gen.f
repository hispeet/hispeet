!> summary:   3d generic element diffusion operator (RLCI)
!> author:    Joerg Stiller
!> date:      2021/06/09
!> license:   Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Diffusion__3D_RLCI__Gen
  use TPO__Diffusion__3D_RLCI__Gen_RDP
end module TPO__Diffusion__3D_RLCI__Gen
