!> summary:   element diffusion operator based on handcrafted kernels (DLCI)
!> author:    Jerome Michel, Joerg Stiller
!> date:      2021/10/12
!> license:   Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Diffusion__3D_DLCI__Hand
  use TPO__Diffusion__3D_DLCI__Hand_RDP
end module TPO__Diffusion__3D_DLCI__Hand
