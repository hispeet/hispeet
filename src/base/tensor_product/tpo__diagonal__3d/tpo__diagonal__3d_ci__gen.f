!> summary:  3D generic scaled constant isotropic diagonal operator
!> author:   Joerg Stiller
!> date:     2020/06/04
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Diagonal__3D_CI__Gen
  use TPO__Diagonal__3D_CI__Gen_RDP
end module TPO__Diagonal__3D_CI__Gen
