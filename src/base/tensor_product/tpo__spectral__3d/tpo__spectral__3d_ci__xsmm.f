!> summary:  3d generic isotropic spectral operator using LIBXSMM
!> author:   Erik Pfister, Joerg Stiller
!> date:     2020/06/01
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Spectral__3D_CI__XSMM
  use TPO__Spectral__3D_CI__XSMM_RDP
end module TPO__Spectral__3D_CI__XSMM
