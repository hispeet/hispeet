!> summary:  3d generic isotropic spectral operator
!> author:   Joerg Stiller
!> date:     2020/05/28
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Spectral__3D_CI__Gen
  use TPO__Spectral__3D_CI__Gen_RDP
end module TPO__Spectral__3D_CI__Gen

