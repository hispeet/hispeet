!> summary:   3d isotropic spectral operator based on handcrafted kernels (I)
!> author:    Joerg Stiller, Erik Pfister
!> date:      2020/05/28
!> license:   Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Spectral__3D_CI__Hand
  use TPO__Spectral__3D_CI__Hand_RDP
end module TPO__Spectral__3D_CI__Hand
