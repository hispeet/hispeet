!> summary:  3d generic anisotropic spectral operator using LIBXSMM
!> author:   Erik Pfister, Joerg Stiller
!> date:     2020/06/02
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Spectral__3D_CA__XSMM
  use TPO__Spectral__3D_CA__XSMM_RDP
end module TPO__Spectral__3D_CA__XSMM
