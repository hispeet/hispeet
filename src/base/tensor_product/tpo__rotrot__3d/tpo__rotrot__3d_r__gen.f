!> summary:  Generic rotrot of a vector field: 3D Cartesian equidistant
!> author:    Jörg Stiller
!> date:      2020/05/07
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__RotRot__3D_R__Gen
  use TPO__RotRot__3D_R__Gen_RDP
end module TPO__RotRot__3D_R__Gen
