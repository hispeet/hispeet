!> summary:  3D rotrot element operator using LIBXSSM (R)
!> author:   Joerg Stiller
!> date:     2021/06/09
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__RotRot__3D_R__XSMM
  use TPO__RotRot__3D_R__XSMM_RDP
end module TPO__RotRot__3D_R__XSMM
