!> summary:  3d generic AxAxA operator using LIBXSMM
!> author:   Joerg Stiller
!> date:     2021/06/09
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__AAA__3D__XSMM
  use TPO__AAA__3D__XSMM_RDP
end module TPO__AAA__3D__XSMM
