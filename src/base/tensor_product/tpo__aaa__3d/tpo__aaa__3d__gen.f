!> summary:   3d generic AxAxA operator
!> author:    Jörg Stiller
!> date:      2020/05/07
!> license:   Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__AAA__3D__Gen
  use TPO__AAA__3D__Gen_RDP
end module TPO__AAA__3D__Gen
