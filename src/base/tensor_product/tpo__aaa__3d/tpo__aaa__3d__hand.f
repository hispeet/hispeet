!> summary:   AxAxA kernel based on handcrafted kernels
!> author:    Joerg Stiller
!> date:      2021/06/09
!> license:   Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__AAA__3D__Hand
  use TPO__AAA__3D__Hand_RDP
end module TPO__AAA__3D__Hand
