!> summary:  3d generic curvilinear element stress operator (DLCI)
!> author:   Joerg Stiller
!> date:     2022/01/02
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Stress__3D_DLCI__Gen
  use TPO__Stress__3D_DLCI__Gen_RDP
end module TPO__Stress__3D_DLCI__Gen
