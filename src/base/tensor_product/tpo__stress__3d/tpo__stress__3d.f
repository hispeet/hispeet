!> summary:  3d element stress operator
!> author:   Joerg Stiller
!> date:     2022/01/03
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Stress__3D
  use TPO__Stress__3D_DLCI
end module TPO__Stress__3D
