!> summary:  3d curvilinear element stress operator with LIBXSMM (DLCI)
!> author:   Joerg Stiller
!> date:     2022/04/19
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Stress__3D_DLCI__XSMM
  use TPO__Stress__3D_DLCI__XSMM_RDP
end module TPO__Stress__3D_DLCI__XSMM
