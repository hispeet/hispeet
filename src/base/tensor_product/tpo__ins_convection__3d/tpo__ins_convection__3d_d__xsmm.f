!> summary:  3D element convection operator for INS using LIBXSMM (D)
!> author:   Jerome Michel
!> date:     2022/01/21
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__INS_Convection__3D_D__XSMM
  use TPO__INS_Convection__3D_D__XSMM_RDP
end module TPO__INS_Convection__3D_D__XSMM
