!> summary:  3D divergence element operator using LIBXSSM for
!> author:   Joerg Stiller
!> date:     2021/06/09
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Div__3D_R__XSMM
  use TPO__Div__3D_R__XSMM_RDP
end module TPO__Div__3D_R__XSMM
