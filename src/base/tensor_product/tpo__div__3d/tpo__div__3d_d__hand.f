!> summary:   Divergence element operator based on handcrafted kernels (D)
!> author:    Jerome Michel
!> date:      2021/10/12
!> license:   Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Div__3D_D__Hand
  use TPO__Div__3D_D__Hand_RDP
end module TPO__Div__3D_D__Hand
