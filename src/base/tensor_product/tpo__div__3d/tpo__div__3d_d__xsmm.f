!> summary:  3D divergence element operator using LIBXSMM (D)
!> author:   Jerome Michel, Joerg Stiller
!> date:     2021/08/17
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Div__3D_D__XSMM
  use TPO__Div__3D_D__XSMM_RDP
end module TPO__Div__3D_D__XSMM
