!> summary:   Divergence element operator based on handcrafted kernels (R)
!> author:    Erik Pfister
!> date:      2020/03/20
!> license:   Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Div__3D_R__Hand
  use TPO__Div__3D_R__Hand_RDP
end module TPO__Div__3D_R__Hand
