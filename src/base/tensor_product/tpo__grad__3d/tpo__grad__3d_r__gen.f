!> summary:  Generic gradient of a vector field: 3D Cartesian equidistant
!> author:   Joerg Stiller
!> date:     2021/016/09
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Grad__3D_R__Gen
  use TPO__Grad__3D_R__Gen_RDP
end module TPO__Grad__3D_R__Gen
