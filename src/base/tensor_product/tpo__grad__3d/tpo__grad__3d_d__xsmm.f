!> summary:  3D gradient element operator using LIBXSMM (D)
!> author:   Jerome Michel, Joerg Stiller
!> date:     2021/08/17
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Grad__3D_D__XSMM
  use TPO__Grad__3D_D__XSMM_RDP
end module TPO__Grad__3D_D__XSMM
