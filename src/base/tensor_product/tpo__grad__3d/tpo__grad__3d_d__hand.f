!> summary:   Gradient element operator based on handcrafted kernels (D)
!> author:    Jerome Michel, Joerg Stiller
!> date:      2021/10/12
!> license:   Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

module TPO__Grad__3D_D__Hand
  use TPO__Grad__3D_D__Hand_RDP
end module TPO__Grad__3D_D__Hand
