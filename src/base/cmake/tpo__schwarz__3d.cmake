set( TPO__SCHWARZ__3D_DIR tensor_product/tpo__schwarz__3d )

set(TENSOR_PRODUCT ${TENSOR_PRODUCT}
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d.f
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ca.F
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ca__gen.f
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ca__gen_rdp.F
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ca__gen_rsp.F
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ca__xsmm.f
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ca__xsmm_rdp.F
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ca__xsmm_rsp.F
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ci.F
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ci__gen.f
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ci__gen_rdp.F
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ci__gen_rsp.F
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ci__hand.f
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ci__hand_rdp.F
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ci__hand_rsp.F
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ci__xsmm.f
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ci__xsmm_rdp.F
                   ${TPO__SCHWARZ__3D_DIR}/tpo__schwarz__3d_ci__xsmm_rsp.F
                   )
