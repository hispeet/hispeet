set( TPO__DIAGONAL__3D_DIR tensor_product/tpo__diagonal__3d )

set(TENSOR_PRODUCT ${TENSOR_PRODUCT}
                   ${TPO__DIAGONAL__3D_DIR}/tpo__diagonal__3d.f
                   ${TPO__DIAGONAL__3D_DIR}/tpo__diagonal__3d_ci.f
                   ${TPO__DIAGONAL__3D_DIR}/tpo__diagonal__3d_ci__gen.f
                   ${TPO__DIAGONAL__3D_DIR}/tpo__diagonal__3d_ci__gen_rdp.F
                   )
