set( TPO__AAA__3D_DIR tensor_product/tpo__aaa__3d )

set(TENSOR_PRODUCT ${TENSOR_PRODUCT}
                   ${TPO__AAA__3D_DIR}/tpo__aaa__3d.F
                   ${TPO__AAA__3D_DIR}/tpo__aaa__3d__gen.f
                   ${TPO__AAA__3D_DIR}/tpo__aaa__3d__gen_rdp.F
                   ${TPO__AAA__3D_DIR}/tpo__aaa__3d__hand.f
                   ${TPO__AAA__3D_DIR}/tpo__aaa__3d__hand_rdp.F
                   ${TPO__AAA__3D_DIR}/tpo__aaa__3d__xsmm.f
                   ${TPO__AAA__3D_DIR}/tpo__aaa__3d__xsmm_rdp.F
                   )
