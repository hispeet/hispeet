set( TPO__DIFFUSION__3D_DIR tensor_product/tpo__diffusion__3d )

set(TENSOR_PRODUCT ${TENSOR_PRODUCT}
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d.f
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_rlci.F
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_rlci__gen.f
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_rlci__gen_rdp.F
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_rlci__hand.f
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_rlci__hand_rdp.F
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_rlci__xsmm.f
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_rlci__xsmm_rdp.F
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_rlvi.F
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_rlvi__gen.f
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_rlvi__gen_rdp.F
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_rlvi__hand.f
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_rlvi__hand_rdp.F
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_rlvi__xsmm.f
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_rlvi__xsmm_rdp.F
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_dlci.F
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_dlci__gen.f
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_dlci__gen_rdp.F
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_dlci__hand.f
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_dlci__hand_rdp.F
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_dlci__xsmm.f
                   ${TPO__DIFFUSION__3D_DIR}/tpo__diffusion__3d_dlci__xsmm_rdp.F
                   )
