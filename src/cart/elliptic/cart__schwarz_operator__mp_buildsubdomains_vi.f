!> summary:  Build Schwarz subdomains for variable isotropic coefficients
!> author:   Joerg Stiller
!> date:     2018/12/25
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!===============================================================================

submodule(CART__Schwarz_Operator) MP_BuildSubdomains_VI
  implicit none

contains

  !-----------------------------------------------------------------------------
  !> Set subdomain configurations and inverse 3D eigenvalues: variable isotropic

  module subroutine BuildSubdomains_VI(this, eop, mesh, lambda, nu, bc)
    class(SchwarzOperator3D),   intent(inout) :: this  !< Schwarz operator
    class(StandardOperators_1D), intent(in)    :: eop   !< 1D SE operators
    class(MeshPartition),       intent(in)    :: mesh  !< mesh partition
    real(RNP), intent(in) :: lambda                    !< Helmholtz parameter
    real(RNP), intent(in) :: nu(0:,0:,0:,:)            !< diffusivity
    character, intent(in) :: bc(:)                     !< BC {'D','N'}

    character :: bc_face(size(bc))
    integer   :: e, i, j, k, n1, n2, n3, po
    real(RNP) :: g0, g1, g2, g3, nu_0
    real(RNP), allocatable :: A(:,:,:)

    !$omp barrier

    ! preliminaries ............................................................

    n1 = size(this % V1, 1)
    n2 = size(this % V2, 1)
    n3 = size(this % V3, 1)
    po = eop % po

    !$omp single

    if (allocated(this%cfg)) then
      if (any(shape(this%cfg) /= [ 3, mesh%ne ])) then
        deallocate(this%cfg)
      end if
    end if

    if (allocated(this%D_inv)) then
      if (any(shape(this%D_inv) /= [ n1, n2, n3, mesh%ne ])) then
        deallocate(this%D_inv)
      end if
    end if

    if (.not. allocated(this%cfg)) then
      allocate(this%cfg(3, mesh%ne))
    end if

    if (.not. allocated(this%D_inv)) then
      allocate(this%D_inv(n1, n2, n3, mesh%ne))
    end if

    !$omp end single

    ! averaging operator
    allocate(A(0:po,0:po,0:po))
    associate(w => eop % w)
      do k = 0, po
      do j = 0, po
      do i = 0, po
        A(i,j,k) = ONE/8 * w(i) * w(j) * w(k)
      end do
      end do
      end do
    end associate

    ! cfg and D_inv ............................................................

    associate( V1    => this % V1,     &
               V2    => this % V2,     &
               V3    => this % V3,     &
               cfg   => this % cfg,    &
               D_inv => this % D_inv,  &
               dx    => mesh % dx      )

      !$omp do
      do e = 1, mesh%ne

        associate(face => mesh % element(e) % face(1:6))
          where(face(:)%boundary > 0)
            bc_face = bc(face%boundary)
          elsewhere
            bc_face = ''
          end where
        end associate

        cfg(1,e) = ConfigurationID(bc_face(1:2))
        cfg(2,e) = ConfigurationID(bc_face(3:4))
        cfg(3,e) = ConfigurationID(bc_face(5:6))

        g0 = dx(1) * dx(2) * dx(3)
        g1 = dx(2) * dx(3) / dx(1)
        g2 = dx(3) * dx(1) / dx(2)
        g3 = dx(1) * dx(2) / dx(3)

        ! mean diffusivity
        nu_0 = 0
        do k = 0, po
        do j = 0, po
        do i = 0, po
          nu_0 = nu_0 + A(i,j,k) * nu(i,j,k,e)
        end do
        end do
        end do

        do k = 1, n3
        do j = 1, n2
        do i = 1, n1

          D_inv(i,j,k,e) = ONE / ( lambda * g0                    &
                                 + nu_0 * ( g1 * V1(i, cfg(1,e))  &
                                          + g2 * V2(j, cfg(2,e))  &
                                          + g3 * V3(k, cfg(3,e))  &
                                          )                       &
                                 )
        end do
        end do
        end do

      end do

    end associate

  end subroutine BuildSubdomains_VI

  !=============================================================================

end submodule MP_BuildSubdomains_VI
