!> summary:  Application of the IP/DG elliptic operator with const diffusivity
!> author:   Joerg Stiller, Gustav Tschirschnitz, Erik Pfister
!> date:     2018/11/22
!> license:  Institute of Fluid Mechanics, TU Dresden, 01062 Dresden, Germany
!>
!> Application of the IP/DG elliptic operator with constant diffusivity
!> and constant isotropic spectral diffusivity, if given
!===============================================================================

submodule(CART__Elliptic_Operator_IP:MP_Apply) MP_Apply_CI
  use TPO__Diffusion__3D_RLCI
  use CART__Trace_Transfer_Buffer
  implicit none

contains

!-------------------------------------------------------------------------------
!> Application of the operator with constant isotropic diffusivity and constant
!> isotropic spectral diffusivity, if given

module subroutine Apply_CI(this, u, v)
  class(EllipticOperator3D_IP), intent(in) :: this
  real(RNP), intent(in)  :: u(0:,0:,0:,:)  !< approximate solution
  real(RNP), intent(out) :: v(0:,0:,0:,:)  !< result

  ! local variables ............................................................

  ! trace operators
  type(TraceTransferBuffer), asynchronous, allocatable, save ::  u_trace_buf
  type(TraceTransferBuffer), asynchronous, allocatable, save :: qn_trace_buf

  ! trace variables
  real(RNP), allocatable, save :: tr_u (:,:,:,:)
  real(RNP), allocatable, save :: tr_qn(:,:,:,:)

  ! 1D standard operators
  real(RNP) :: As(0:this%eop%po, 0:this%eop%po) ! diffusion Dᵀ(ν+νˢQ)D
  real(RNP) :: Bs(0:this%eop%po, 0:this%eop%po) ! "flux" (ν+νˢQ)D

  integer   :: po, ne, nf, np
  real(RNP) :: nu_svv

  select type(eop => this % eop)
  class is (DG_ElementOperators_1D)

    associate(mesh => this % mesh, lambda => this % lambda, nu => this % nu_ci)

      ! initialization .........................................................

      po = eop  % po
      ne = mesh % ne
      nf = mesh % nf
      np = po + 1

      ! computation of 1D standard diffusion and standard flux operator
      if (eop % Has_SVV()) then
        call eop % Get_SVV_StandardStiffnessMatrix(As)
        call eop % Get_SVV_StandardDiffMatrix(Bs)
        nu_svv = this % nu_ci_svv
        As     = nu_svv * As
        Bs     = nu_svv * Bs
      else
        nu_svv = ZERO
        As     = ZERO
        Bs     = ZERO
      end if

      As = As + nu * eop%L
      Bs = Bs + nu * eop%D

      ! workspace and operators
      !$omp master                                                            !1
      allocate(tr_u(0:po, 0:po, 2, mesh%nf))                                  !1
      allocate(tr_qn, mold=tr_u)                                              !1
      u_trace_buf  = TraceTransferBuffer(mesh, tr_u)                          !1
      qn_trace_buf = TraceTransferBuffer(mesh, tr_qn)                         !1
      !$omp end master                                                        !1
      !$omp barrier                                                           !1

      ! start generation of traces .............................................

      ! initialize, compute and transfer face normal fluxes qn
      call GetLocalTraces(mesh, np, ne, nf, Bs, mesh%dx, u, tr_u, tr_qn)      !3
      call  u_trace_buf % Transfer(mesh, tr_u , tag=1000)                     !9
      call qn_trace_buf % Transfer(mesh, tr_qn, tag=2000)                     !9

      ! apply element diffusion operator .......................................

      call TPO_Diffusion(eop%w, As, mesh%dx, lambda, ONE, u, v)

      ! finish generation of traces ............................................

      call  u_trace_buf % Merge(mesh, tr_u , alpha = ZERO, beta = ONE )       !9
      call qn_trace_buf % Merge(mesh, tr_qn, alpha = ZERO, beta = ONE)        !9

      call ApplyBoundaryConditions(mesh, this%bc, tr_u, tr_qn)                !5

      ! add fluxes .............................................................

      call AddFluxes(mesh, eop, Bs, nu, nu_svv, tr_u, tr_qn, v)               !4

      ! clean-up ...............................................................

      !$omp master                                                            !1
      deallocate(tr_u, tr_qn)                                                 !1
      deallocate(u_trace_buf, qn_trace_buf)                                   !1
      !$omp end master

    end associate
  end select

end subroutine Apply_CI

!-------------------------------------------------------------------------------
!> Compute and add fluxes through element boundaries

subroutine AddFluxes(mesh, eop, Bs, nu, nu_svv, tr_u, tr_qn, v)

  ! arguments ..................................................................

  class(MeshPartition),          intent(in) :: mesh !< mesh partition
  class(DG_ElementOperators_1D), intent(in) :: eop  !< ID/DG element operators

  real(RNP), intent(in)    :: Bs(0:,0:)        !< 1D standard "flux" operator
  real(RNP), intent(in)    :: nu               !< diffusivity
  real(RNP), intent(in)    :: nu_svv           !< spectral diffusivity
  real(RNP), intent(in)    :: tr_u (0:,0:,:,:) !< [u]ᵢ
  real(RNP), intent(in)    :: tr_qn(0:,0:,:,:) !< {q}ᵢ
  real(RNP), intent(inout) :: v(0:,0:,0:,:)    !< result

  ! local variables ............................................................

  real(RNP), dimension(0:eop%po, 0:eop%po) :: Mf1, Mf2, Mf3
  real(RNP), dimension(0:eop%po, 0:eop%po) :: Aq_0, Aq_P, Ju_0, Ju_P
  real(RNP), dimension(0:eop%po)           :: delta_0, delta_P
  real(RNP) :: g(3), mu(3)
  real(RNP) :: cd_0, cd_P, cp_0, cp_P
  integer   :: i, j, k, e, f(6), f_0, f_P

  associate( P  => eop  % po, &
             Ms => eop  % w,  &
             dx => mesh % dx  )

    ! auxiliaries .............................................................

    ! face mass matrices

    g(1) = dx(2) * dx(3) / 4
    g(2) = dx(3) * dx(1) / 4
    g(3) = dx(1) * dx(2) / 4

    do j = 0, P
    do i = 0, P
      Mf1(i,j) = g(1) * Ms(i) * Ms(j)
      Mf2(i,j) = g(2) * Ms(i) * Ms(j)
      Mf3(i,j) = g(3) * Ms(i) * Ms(j)
    end do
    end do

    ! delta function
    delta_0 = [ ONE, (ZERO, i=1,P) ]
    delta_P = [ (ZERO, i=1,P), ONE ]

    ! penalties
    mu(1) = eop % PenaltyFactor(dx(1))
    mu(2) = eop % PenaltyFactor(dx(2))
    mu(3) = eop % PenaltyFactor(dx(3))

    ! add fluxes ...............................................................

    g = ONE / dx

    !$omp do private(e)
    do e = 1, mesh % ne

      f = mesh % element(e) % face % id

      ! direction 1: v = v + Mf₁ (-[ϕ]₁{(ν+νˢQ)∇u}₁ - {(ν+νˢQ)∇ϕ}₁[u]₁
      !                           + μ⟨ν+νˢ⟩[ϕ]₁[u]₁)
      !
      ! where, with ϕ = ℓ_ijk, at  ξ = -1
      !
      !   [ϕ]₁         = -delta_0(i)
      !   {(ν+νˢQ)∇ϕ}₁ = g(1) * Bs(0,i)
      !   [u]₁         = jmp( u (j,k,f₁) )
      !   {(ν+νˢQ)∇u}₁ = avg( q₁(j,k,f₁) )
      !
      ! and, at  ξ = +1
      !
      !   [ϕ]₁         = delta_P(i)
      !   {(ν+νˢQ)∇ϕ}₁ = g(1) * Bs(P,i)
      !   [u]₁         = jmp( u (j,k,f₂) )
      !   {(ν+νˢQ)∇u}₁ = avg( q₁(j,k,f₂) )

      f_0 = f(1)
      f_P = f(2)

      cd_0 = -g(1)
      cd_P = -g(1)
      cp_0 = -(nu + nu_svv) * mu(1)
      cp_P =  (nu + nu_svv) * mu(1)

      Aq_0 = HALF * (tr_qn(:,:,1,f_0) - tr_qn(:,:,2,f_0))
      Aq_P = HALF * (tr_qn(:,:,1,f_P) - tr_qn(:,:,2,f_P))

      Ju_0 = tr_u(:,:,1,f_0) - tr_u(:,:,2,f_0)
      Ju_P = tr_u(:,:,1,f_P) - tr_u(:,:,2,f_P)

      do k = 0, P
      do j = 0, P
      do i = 0, P

        v(i,j,k,e) = v(i,j,k,e)                                           &
          + Mf1(j,k) * ( delta_0(i) * Aq_0(j,k)                           &
                       - delta_P(i) * Aq_P(j,k)                           &
                       + (cd_0 * Bs(0,i) + cp_0 * delta_0(i)) * Ju_0(j,k) &
                       + (cd_P * Bs(P,i) + cp_P * delta_P(i)) * Ju_P(j,k) )
      end do
      end do
      end do

      ! direction 2: v = v + Mf₂ (-[ϕ]₂{(ν+νˢQ)∇u}₂ - {(ν+νˢQ)∇ϕ}₂[u]₂
      !                           + μ⟨ν+νˢ⟩[ϕ]₂[u]₂)

      f_0 = f(3)
      f_P = f(4)

      cd_0 = -g(2)
      cd_P = -g(2)
      cp_0 = -(nu + nu_svv) * mu(2)
      cp_P =  (nu + nu_svv) * mu(2)

      Aq_0 = HALF * (tr_qn(:,:,1,f_0) - tr_qn(:,:,2,f_0))
      Aq_P = HALF * (tr_qn(:,:,1,f_P) - tr_qn(:,:,2,f_P))

      Ju_0 = tr_u(:,:,1,f_0) - tr_u(:,:,2,f_0)
      Ju_P = tr_u(:,:,1,f_P) - tr_u(:,:,2,f_P)

      do k = 0, P
      do j = 0, P
      do i = 0, P

        v(i,j,k,e) = v(i,j,k,e)                                           &
          + Mf2(i,k) * ( delta_0(j) * Aq_0(i,k)                           &
                       - delta_P(j) * Aq_P(i,k)                           &
                       + (cd_0 * Bs(0,j) + cp_0 * delta_0(j)) * Ju_0(i,k) &
                       + (cd_P * Bs(P,j) + cp_P * delta_P(j)) * Ju_P(i,k) )
      end do
      end do
      end do

      ! direction 3: v = v + Mf₃ (-[ϕ]₃{(ν+νˢQ)∇u}₃ - {(ν+νˢQ)∇ϕ}₃[u]₃
      !                           + μ⟨ν+νˢ⟩[ϕ]₃[u]₃)

      f_0 = f(5)
      f_P = f(6)

      cd_0 = -g(3)
      cd_P = -g(3)
      cp_0 = -(nu + nu_svv) * mu(3)
      cp_P =  (nu + nu_svv) * mu(3)

      Aq_0 = HALF * (tr_qn(:,:,1,f_0) - tr_qn(:,:,2,f_0))
      Aq_P = HALF * (tr_qn(:,:,1,f_P) - tr_qn(:,:,2,f_P))

      Ju_0 = tr_u(:,:,1,f_0) - tr_u(:,:,2,f_0)
      Ju_P = tr_u(:,:,1,f_P) - tr_u(:,:,2,f_P)

      do k = 0, P
      do j = 0, P
      do i = 0, P

        v(i,j,k,e) = v(i,j,k,e)                                           &
          + Mf3(i,j) * ( delta_0(k) * Aq_0(i,j)                           &
                       - delta_P(k) * Aq_P(i,j)                           &
                       + (cd_0 * Bs(0,k) + cp_0 * delta_0(k)) * Ju_0(i,j) &
                       + (cd_P * Bs(P,k) + cp_P * delta_P(k)) * Ju_P(i,j) )
      end do
      end do
      end do

    end do

  end associate

end subroutine AddFluxes

!-------------------------------------------------------------------------------
!> Elementwise computation of diffusive fluxes parallel to face normals
!>
!> Computes the normal components of (ν+νˢQ) grad(u) for all element boundary
!> points. Entries corresponding to interior points or tangential components are
!> set to zero.

subroutine GetLocalTraces(mesh, np, ne, nf, Bs, dx, u, tr_u, tr_qn)
  class(MeshPartition), intent(in) :: mesh      !< mesh partition
  integer,   intent(in)    :: np                !< number of points per direction
  integer,   intent(in)    :: ne                !< number of elements
  integer,   intent(in)    :: nf                !< number of faces
  real(RNP), intent(in)    :: Bs(np,np)         !< 1D standard "flux" operator
  real(RNP), intent(in)    :: dx(3)             !< element extensions
  real(RNP), intent(in)    :: u(np,np,np,ne)    !< 3D scalar field
  real(RNP), intent(inout) :: tr_u (np,np,2,nf) !< solution traces
  real(RNP), intent(inout) :: tr_qn(np,np,2,nf) !< normal flux traces

  real(RNP) :: Bs_f(np,2)
  real(RNP) :: g(3), tmp1, tmp2
  integer   :: e, f(6), i, j, k, m

  ! initialization .............................................................

  ! transposed normal differentiation operators for first and last point
  Bs_f(:,1) = -Bs( 1,:)  ! first
  Bs_f(:,2) =  Bs(np,:)  ! last

  ! metric coefficients
  g = 2 / dx

  !$acc data present(u,q) copyin(Bs_f,g)
  !$acc parallel
  !$acc loop gang worker

  !$omp do private(e)
  do e = 1, ne

    f = mesh % element(e) % face % id

    ! qn(*,f(1:2)) = ∓(ν+νˢQ) ∂u/∂x1 ...........................................

    !$acc loop collapse(2) vector
    do k = 1, np
    do j = 1, np

      tmp1 = 0
      tmp2 = 0
      do m = 1, np
        tmp1 = tmp1 + Bs_f(m,1) * u(m,j,k,e)
        tmp2 = tmp2 + Bs_f(m,2) * u(m,j,k,e)
      end do
      tr_u (j,k,2,f(1)) = u( 1,j,k,e)
      tr_u (j,k,1,f(2)) = u(np,j,k,e)
      tr_qn(j,k,2,f(1)) = g(1) * tmp1
      tr_qn(j,k,1,f(2)) = g(1) * tmp2

    end do
    end do

    ! qn(*,f(3:4)) = ∓(ν+νˢQ) ∂u/∂x2 ...........................................

    !$acc loop collapse(2) vector
    do k = 1, np
    do i = 1, np

      tmp1 = 0
      tmp2 = 0
      do m = 1, np
        tmp1 = tmp1 + Bs_f(m,1) * u(i,m,k,e)
        tmp2 = tmp2 + Bs_f(m,2) * u(i,m,k,e)
      end do
      tr_u (i,k,2,f(3)) = u(i, 1,k,e)
      tr_u (i,k,1,f(4)) = u(i,np,k,e)
      tr_qn(i,k,2,f(3)) = g(2) * tmp1
      tr_qn(i,k,1,f(4)) = g(2) * tmp2

    end do
    end do

    ! qn(*,f(5:6)) = ∓(ν+νˢQ) ∂u/∂x3 ...........................................

    !$acc loop collapse(2) vector
    do j = 1, np
    do i = 1, np

      tmp1 = 0
      tmp2 = 0
      do m = 1, np
        tmp1 = tmp1 + Bs_f(m,1) * u(i,j,m,e)
        tmp2 = tmp2 + Bs_f(m,2) * u(i,j,m,e)
      end do
      tr_u (i,j,2,f(5)) = u(i,j, 1,e)
      tr_u (i,j,1,f(6)) = u(i,j,np,e)
      tr_qn(i,j,2,f(5)) = g(3) * tmp1
      tr_qn(i,j,1,f(6)) = g(3) * tmp2

    end do
    end do

  end do

  !$acc end parallel
  !$acc end data

end subroutine GetLocalTraces

!===============================================================================

end submodule MP_Apply_CI
