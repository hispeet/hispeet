#!/usr/bin/env bash

# Start jobs for h-convergence study

# P = 6
sbatch p06_n04.slurm
sbatch p06_n05.slurm
sbatch p06_n06.slurm
sbatch p06_n07.slurm
sbatch p06_n08.slurm
sbatch p06_n09.slurm
sbatch p06_n10.slurm
sbatch p06_n11.slurm
sbatch p06_n12.slurm
sbatch p06_n13.slurm
sbatch p06_n14.slurm
sbatch p06_n15.slurm
sbatch p06_n16.slurm

# P = 11
sbatch p11_n02.slurm
sbatch p11_n03.slurm
sbatch p11_n04.slurm
sbatch p11_n05.slurm
sbatch p11_n06.slurm
sbatch p11_n07.slurm
sbatch p11_n08.slurm
sbatch p11_n09.slurm
sbatch p11_n10.slurm
sbatch p11_n11.slurm
sbatch p11_n12.slurm

# P = 16
sbatch p16_n02.slurm
sbatch p16_n03.slurm
sbatch p16_n04.slurm
sbatch p16_n05.slurm
sbatch p16_n06.slurm
sbatch p16_n07.slurm
sbatch p16_n08.slurm
