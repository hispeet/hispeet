### Names


Name     | Short   | Meaning
-------- |:-------:| -------------------------------------------
n_bound  | nb      | number of boundaries
n_comp   | nc      | number of components (of a variable)
n_edge   | ne      | number of edges
n_elem   | ne      | number of elements
n_face   | nf      | number of faces
n_ghost  | ng      | number of ghost elements
n_part   |         | number of partitions
n_vert   | nv      | number of vertices
np       |       | number of collocation points per direction
np_elem  | np_e    | number of element points
np_mesh  | np_m    | number of mesh points
nq       |         | number of quadrature points per direction
po       |         | polynomial order


Take care that the short version is only used, whe the context is clear!

