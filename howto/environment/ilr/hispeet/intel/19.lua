load("openmpi_intel")

setenv("CC",  "mpicc")
setenv("CXX", "mpicxx")
setenv("FC",  "mpifort")

prepend_path ("LD_LIBRARY_PATH","/mnt/appl/x86_64/intel/parallel_studio_xe_2019.2.057/compilers_and_libraries_2019/linux/mkl/lib/intel64")

