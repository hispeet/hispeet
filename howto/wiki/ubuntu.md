## Ubuntu

Installing _HiSPEET_ on Ubuntu 20 requires the packages

- git
- cmake
- gcc
- g++
- gfortran
- libvtk7-dev
- make
- openmpi-bin
- python3
- python3-matplotlib
- python3-numpy
- libopenblas-dev
- libfftw3-dev

The following packages are optional but strongly recommended

- lmod
- paraview
- kate

For installing a package you may use the command

    sudo apt-get install <package name>

