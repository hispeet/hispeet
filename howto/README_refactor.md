* `Standard_Operators__1D: StandardOperators_1D`

        % xi   →  % x
        % V    →  % GetVandermondeMatrix(V)
        % VI   →  % GetInverseVandermondeMatrix(VI)

* `DG_Element_Operators_1D: DG_ElementOperators1D `

        PenaltyFactor          →  % PenaltyFactor
        Get_StiffnessMatrix1D  →  % Get_1D_StiffnessMatrix

* `CART__Boundary_Variable: BoundaryVariable`

        % ExtractNormalComponent  →  % Extract
