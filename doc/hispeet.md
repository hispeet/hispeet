project:          HiSPEET
summary:          HiSPEET - High Performance SPEctral Element Techniques.
author:           Jörg Stiller
css:              hispeet.css
src_dir:          ../src
                  ../example
output_dir:       ./html
fixed_extensions: for
                  FOR
extensions:       f
                  ft
fpp_extensions:   F
preprocess:       false
display:          public
                  protected
                  private
source:           false
graph:            true
search:           true
docmark:          <
<!--
extra_filetypes: c   //
                 sh  #
                 py  #
-->

